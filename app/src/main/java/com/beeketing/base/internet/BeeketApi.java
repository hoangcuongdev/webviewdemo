package com.beeketing.base.internet;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by cuonghh on 2019-07-20
 */
public interface BeeketApi {
    Observable<Object> login(String username, String passs);
}
