package com.beeketing.base.internet;

import android.content.Context;
import androidx.annotation.Nullable;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by cuonghh on 2019-07-20
 */
public class HttpInterceptor implements Interceptor {
    private Context context;
    public HttpInterceptor(Context mContext) {
        this.context = mContext;
    }

    @Override
    public Response intercept(@Nullable Chain chain) throws IOException {
        return chain.proceed(chain.request());
    }
}
