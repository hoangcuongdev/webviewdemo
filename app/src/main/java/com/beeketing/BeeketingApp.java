package com.beeketing;

import android.app.Application;
import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cuonghh on 2019-09-11
 */
public class BeeketingApp extends Application {
    protected static BeeketingApp INSTANCE;
    private Retrofit mRetrofit;
    private static final String END_POINT = "https://amduongnguhanh.vn";
    private static Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        gson = new Gson();

        OkHttpClient mClient = new OkHttpClient.Builder()
                .readTimeout(120, TimeUnit.SECONDS)
                .connectTimeout(120, TimeUnit.SECONDS)
                .build();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(END_POINT)
                .client(mClient)
//                .addConverterFactory(SimpleXmlConverterFactory.createNonStrict(
//                        new Persister(new AnnotationStrategy() // important part!
//                        )))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }
    public static synchronized BeeketingApp getInstance() {
        return INSTANCE;
    }

    public Gson getGson(){
        return gson;
    }

    public synchronized <T> T createRetrofitService(Class<T> clazz) {
        return mRetrofit.create(clazz);
    }
}
