package com.beeketing.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import butterknife.BindView;
import com.beeketing.R;
import com.beeketing.base.BaseActivity;

/**
 * Created by cuonghh on 2019-09-11
 */
public class WebviewActivity extends BaseActivity {

    public static final String BUNDLE_DATA = "web_data";
    public static final String BUNDLE_URL = "web_url";
    public static final String BUNDLE_IS_URL = "web_is_url";

    @Override
    protected int layoutId() {
        return R.layout.activity_webview;
    }
    @BindView(R.id.webview)
    WebView webView;


    @Override
    protected void onResume() {
        super.onResume();
        Bundle bundle = getIntent().getBundleExtra(BUNDLE_DATA);
        if(null != bundle){
            String url = bundle.getString(BUNDLE_URL);
            boolean isUrl = bundle.getBoolean(BUNDLE_IS_URL);
            setting();
            loadWebview(url,isUrl);
        }


    }

    private void setting(){
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAppCacheEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);

        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        settings.setDisplayZoomControls(false);

        settings.setTextZoom(125);

        settings.setBlockNetworkImage(false);
        settings.setLoadsImagesAutomatically(true);

        if(VERSION.SDK_INT >= VERSION_CODES.O){
            settings.setSafeBrowsingEnabled(false);
        }
        settings.setUseWideViewPort(false);
        settings.setLoadWithOverviewMode(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setMediaPlaybackRequiresUserGesture(false);

        settings.setDomStorageEnabled(true);
        settings.setSupportMultipleWindows(true);
        settings.setLoadWithOverviewMode(true);
        settings.setAllowContentAccess(true);
        settings.setAllowFileAccess(true);
    }

    private void loadWebview(String url, boolean isUrl){
        webView.setFitsSystemWindows(true);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE,null);

        if(isUrl){
            webView.loadUrl(url);
        }else {
            webView.loadDataWithBaseURL("about:blank",url,"text/html","utf-8",null);
            webView.setMinimumHeight(200);
        }
    }
}
