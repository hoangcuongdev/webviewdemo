package com.beeketing.ui;

import static com.beeketing.ui.WebviewActivity.BUNDLE_DATA;
import static com.beeketing.ui.WebviewActivity.BUNDLE_IS_URL;
import static com.beeketing.ui.WebviewActivity.BUNDLE_URL;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import butterknife.OnClick;
import com.beeketing.R;
import com.beeketing.base.BaseActivity;

public class MainActivity extends BaseActivity {

    @Override
    protected int layoutId() {
        return R.layout.activity_main;
    }

    @OnClick(R.id.btnWebview)
    void onLoadWebview(){
//        gotoWebview("https://amduongnguhanh.vn",true);
        gotoWebview(getString(R.string.html),false);
    }

    public void gotoWebview(String url, boolean isUrl){
        Intent intent = new Intent(this,WebviewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_URL,url);
        bundle.putBoolean(BUNDLE_IS_URL,isUrl);
        startActivity(intent.putExtra(BUNDLE_DATA,bundle));
    }


}
