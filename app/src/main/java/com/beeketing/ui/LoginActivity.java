package com.beeketing.ui;

import android.content.Intent;
import android.widget.EditText;
import butterknife.BindView;
import butterknife.OnClick;
import com.beeketing.BeeketingApp;
import com.beeketing.R;
import com.beeketing.base.BaseActivity;
import com.beeketing.base.internet.BeeketApi;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by cuonghh on 2019-09-11
 */
public class LoginActivity extends BaseActivity {

    @Override
    protected int layoutId() {
        return R.layout.activity_login;
    }

    @BindView(R.id.edtUsername)
    EditText edtUserName;
    @BindView(R.id.edtPass)
    EditText edtPass;
    @OnClick(R.id.btnLogin)
    void onCLickLogin(){
        String userName = edtUserName.getText().toString();
        String pass = edtPass.getText().toString();
//        requestApiLogin(userName,pass);
        launchToMain();
    }

    private void requestApiLogin(String userName, String pass){
        Disposable loginDisposable = BeeketingApp.getInstance()
                .createRetrofitService(BeeketApi.class)
                .login(userName, pass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {
                            //Todo Result
                        },
                        throwable -> {
                            //Todo Exception
                        });
        disposables.add(loginDisposable);
    }
    private void launchToMain(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);
    }
}
